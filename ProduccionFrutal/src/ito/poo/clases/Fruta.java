package ito.poo.clases;

import java.util.ArrayList;

public class Fruta {

		private String nomb;
		private float extens;
		private float costoProm;
		private float precioVPromedio;
		private ArrayList<Periodo> periodos = new ArrayList<Periodo>();
		/*****************************/
		public Fruta() {
			super();
		}

		public Fruta(String nomb, float extens, float costoProm, float precioVPromedio) {
			super();
			this.nomb = nomb;
			this.extens = extens;
			this.costoProm = costoProm;
			this.precioVPromedio = precioVPromedio;
		}
		/*****************************/
		public String getNomb() {
			return nomb;
		}

		public void setNomb(String nomb) {
			this.nomb = nomb;
		}

		private float getExtens() {
			return extens;
		}

		public void setExtension(float extension) {
			this.extens = extension;
		}

		public float getCostoProm() {
			return costoProm;
		}

		public void setCostoProm(float costoProm) {
			this.costoProm= costoProm;
		}

		public float getPrecioVPromedio() {
			return precioVPromedio;
		}

		public void setPrecioVPromedio(float precioVPromedio) {
			this.precioVPromedio = precioVPromedio;
		}

		public ArrayList<Periodo> getPeriodos() {
			return this.periodos;
		}
		/*****************************/
		public void agregarPeriodo(String tiempoCos, float cantCosxtiempo) {
			Periodo p = new Periodo (tiempoCos, cantCosxtiempo);
			this.periodos.add(p);
		}
		
		public Periodo devolvPeriodo(int i) {
			Periodo p;
			if (i > this.periodos.size() || i < 0)
				p = new Periodo ("null", 0);
			else 
				p = this.periodos.get(i);
			return p;
		}

		public boolean elimPeriodo(int i) {
			boolean e;
			if (i > this.periodos.size() || i < 0)
				e = false;
			else {
				this.periodos.remove(i);
				e = true;
			}
			return e;
		}
		
		public float costoPeriodo(Periodo p) {
			float i = 0;
			if (i > this.periodos.size() || i < 0)
				i = 0;
			else 
				i = p.getCantCosXTiempo() * this.costoProm;
			return i;
		}

		public float gananciaEstim(Periodo p) {
			float i = 0;
			if (i > this.periodos.size() || i < 0)
				i = 0;
			else 
				i = (p.getCantCosXTiempo() * this.precioVPromedio) - costoPeriodo(p);;
			return i;
		}
		/*****************************/
		@Override
		public String toString() {
			return "Fruta: " + nomb+ "\nExtens: " + extens + "\nCosto Promedio: " + costoProm
					+ "\nPrecio Venta Promedio: " + precioVPromedio + "\nPeriodos: " + periodos;
		}
		/*****************************/
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Float.floatToIntBits(costoProm);
			result = prime * result + Float.floatToIntBits(extens);
			result = prime * result + ((nomb == null) ? 0 : nomb.hashCode());
			result = prime * result + ((periodos == null) ? 0 : periodos.hashCode());
			result = prime * result + Float.floatToIntBits(precioVPromedio);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Fruta other = (Fruta) obj;
			if (Float.floatToIntBits(costoProm) != Float.floatToIntBits(other.costoProm))
				return false;
			if (Float.floatToIntBits(extens) != Float.floatToIntBits(other.extens))
				return false;
			if (nomb == null) {
				if (other.nomb != null)
					return false;
			} else if (!nomb.equals(other.nomb))
				return false;
			if (periodos == null) {
				if (other.periodos != null)
					return false;
			} else if (!periodos.equals(other.periodos))
				return false;
			if (Float.floatToIntBits(precioVPromedio) != Float.floatToIntBits(other.precioVPromedio))
				return false;
			return true;
		}
		public int compareTo(Fruta arg0) {
			int r=0;
			if (!this.nomb.equals(arg0.getNomb()))
				return this.nomb.compareTo(arg0.getNomb());
			else if (this.extens != arg0.getExtens())
				return this.extens>arg0.getExtens()?1:-1;
			else if (this.precioVPromedio != arg0.getPrecioVPromedio())
				return this.precioVPromedio>arg0.getPrecioVPromedio()?2:-2;
			else if (this.costoProm != arg0.getCostoProm())
				return this.costoProm>arg0.getCostoProm()?3:-3;
			return r;
		}

}
