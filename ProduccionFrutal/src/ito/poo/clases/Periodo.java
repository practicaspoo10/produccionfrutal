package ito.poo.clases;

public class Periodo {
	
	private String tiempoCos;
	private float cantCosxtiempo;
	/*****************************/
	public Periodo() {
		super();
	}
	
	public Periodo(String tiempoCos, float cantCosxtiempo) {
		super();
		this.tiempoCos = tiempoCos;
		this.cantCosxtiempo = cantCosxtiempo;
	}
	/*****************************/
	public String getTiempoCos() {
		return tiempoCos;
	}

	public void setTiempoCos(String tiempoCos) {
		this.tiempoCos = tiempoCos;
	}

	float getCantCosXTiempo() {
		return cantCosxtiempo;
	}

	public void setCantCosxtiempo(float cantCosxtiempo) {
		this.cantCosxtiempo = cantCosxtiempo;
	}
	/*****************************/
	@Override
	public String toString() {
		return "(Tiempo de cosecha: " + tiempoCos + ", Cantidad de cosecha: " + cantCosxtiempo + ")";
	}
	/*****************************/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(cantCosxtiempo);
		result = prime * result + ((tiempoCos == null) ? 0 : tiempoCos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Periodo other = (Periodo) obj;
		if (Float.floatToIntBits(cantCosxtiempo) != Float.floatToIntBits(other.cantCosxtiempo))
			return false;
		if (tiempoCos == null) {
			if (other.tiempoCos != null)
				return false;
		} else if (!tiempoCos.equals(other.tiempoCos))
			return false;
		return true;
	}
	public int compareTo(Periodo arg0) {
		int r=0;
		if (!this.tiempoCos.equals(arg0.getTiempoCos()))
				return this.tiempoCos.compareTo(arg0.getTiempoCos());
		else if (this.cantCosxtiempo != arg0.getCantCosXTiempo())
			return this.cantCosxtiempo>arg0.getCantCosXTiempo()?1:-1;
		return r;
	}
}


