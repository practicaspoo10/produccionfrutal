package ito.poo.app;

import ito.poo.clases.Fruta;
import ito.poo.clases.Periodo;

@SuppressWarnings("unused")

public class MyApp {
	
		static void run() {
			Fruta frut1 = new Fruta("Durazno", 3.5F, 2, 5);
			System.out.println(frut1);
			System.out.println();
			frut1.agregarPeriodo("Un mes", 4000);
			System.out.println(frut1);
			System.out.println();

			Fruta frut2 = new Fruta("Pera", 3F, 1, 6);
			System.out.println(frut2);
			System.out.println();
			frut2.agregarPeriodo("Dos meses", 3000);
			System.out.println(frut2);
			System.out.println();
			
			Fruta frut3 = new Fruta("Kiwi", 3.5F, 2, 5);
			System.out.println(frut3);
			System.out.println();
			frut3.agregarPeriodo("Un mes", 4000);
			System.out.println(frut3);
			System.out.println();
		}
		
		static void run2() {
			Fruta f1 = new Fruta("Durazno", 3.5F, 2, 5);
			System.out.println(f1);
			System.out.println();
			f1.agregarPeriodo("Febrero", 4000);
			f1.agregarPeriodo("Junio y Julio", 7500);
			System.out.println(f1);
			System.out.println();
			System.out.println(f1.getPeriodos());
			System.out.println(f1.devolvPeriodo(3));
			System.out.println(f1.devolvPeriodo(1));
			System.out.println();
			System.out.println(f1.costoPeriodo(f1.devolvPeriodo(3)));
			System.out.println(f1.costoPeriodo(f1.devolvPeriodo(1)));
			System.out.println(f1.gananciaEstim(f1.devolvPeriodo(1)));

			System.out.println();
			System.out.println("/************************/");
			System.out.println();

			Fruta f2 = new Fruta("Pera", 3F, 1, 6);
			System.out.println(f2);
			System.out.println();
			f2.agregarPeriodo("Marzo", 3000);
			f2.agregarPeriodo("Abril y Mayo", 6550);
			System.out.println(f2);
			System.out.println();
			System.out.println(f2.getPeriodos());
			System.out.println(f2.devolvPeriodo(3));
			System.out.println(f2.devolvPeriodo(1));
			System.out.println();
			System.out.println(f2.costoPeriodo(f2.devolvPeriodo(3)));
			System.out.println(f2.costoPeriodo(f2.devolvPeriodo(1)));
			System.out.println(f2.gananciaEstim(f2.devolvPeriodo(1)));

			System.out.println();
			System.out.println("/************************/");
			System.out.println();
			
			
			
			System.out.println(!f1.equals(f2));
		    System.out.println(f2.compareTo(f1));
		    
		    System.out.println(!f1.equals(f2));
		    System.out.println(f2.compareTo(f1));
		}

		public static void main(String[] args) {
			//run();
			run2();
		}
	}

	